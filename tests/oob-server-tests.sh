#!/bin/bash

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR Could not bring up last series of devices, there was an error of some kind!"
    exit 1
fi
}

set -e
set -x

echo "This is from inside the OOB Server"
echo "Put tests we want to run in this script"
